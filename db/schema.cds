using { cuid } from '@sap/cds/common';

entity Foo : cuid{
    value : String;
}
