using {Foo as s} from '../db/schema';

service DemoService {
    action poster(p:String) returns String;
    function getter() returns String;
    entity Foo as projection on s;
}