const cds = require('@sap/cds');
module.exports = async function demoService() {
  const demo = await cds.connect.to("demo1")
  this.on('poster', async (req) => `Hallo ${req.data.p}`)
  this.on('getter', async () => {
    console.log(
      await SELECT.from('Foo')
      // ,await demo.send("poster", { p: 'Welt' }) // <<< uncomment this line
    )
    return 'Nur ein Test'
  })
}